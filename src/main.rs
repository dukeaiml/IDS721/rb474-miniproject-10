use lambda_http::{run, service_fn, tracing, Body, Error, Request, RequestExt, Response};
use std::convert::Infallible;
use std::io::Write;
use std::path::PathBuf;

// Define a function to handle AI model inference, dynamically selecting the model based on parameters
fn handle_inference(prompt: String) -> Result<String, Box<dyn std::error::Error>> {
    let tokenizer = llm::TokenizerSource::Embedded;
    let model_path = PathBuf::from("model/pythia-1b-q4_0-ggjt.bin");
    let model = llm::load_dynamic(
        Some(llm::ModelArchitecture::GptNeoX),
        &model_path,
        tokenizer,
        Default::default(),
        llm::load_progress_callback_stdout,
    )?;

    let mut session = model.start_session(Default::default());
    let mut response_text = String::new();

    let inference_result = session.infer::<Infallible>(
        model.as_ref(),
        &mut rand::thread_rng(),
        &llm::InferenceRequest {
            prompt: (&prompt).into(),
            parameters: &llm::InferenceParameters::default(),
            play_back_previous_tokens: false,
            maximum_token_count: Some(5),
        },
        &mut Default::default(),
        |response| match response {
            llm::InferenceResponse::PromptToken(token) | llm::InferenceResponse::InferredToken(token) => {
                print!("{token}");
                std::io::stdout().flush().unwrap();
                response_text.push_str(&token);
                Ok(llm::InferenceFeedback::Continue)
            }
            _ => Ok(llm::InferenceFeedback::Continue),
        },
    );


    match inference_result {
        Ok(_) => Ok(response_text),
        Err(e) => Err(Box::new(e)),
    }
}

// Define a function to handle HTTP requests, leveraging the AI model
async fn handle_request(req: Request) -> Result<Response<Body>, Box<dyn std::error::Error>> {
    let user_query = req
        .query_string_parameters_ref()
        .and_then(|params| params.first("text"))
        .unwrap_or("How is the weather?");

    let response = match handle_inference(user_query.to_string()) {
        Ok(result) => result,
        Err(e) => format!("Inference error: {:?}", e),
    };

    Response::builder()
        .status(200)
        .header("content-type", "text/html")
        .body(Body::from(response))
        .map_err(|e| Box::new(e) as Box<dyn std::error::Error>)
}


#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();
    run(service_fn(handle_request)).await
}
