# rb474-miniproject-10

## Task
Rust Command-Line Tool with Testing

Requirements
- Dockerize Hugging Face Rust transformer
- Deploy container to AWS Lambda
- Implement query endpoint

## Description
A transformer (language model) accessible serverlessly.

## Usage
To test the functionality of the mini-project, follow the [link](https://5j5nzpzpvz5llkbdmhqpggbzlu0lxstr.lambda-url.us-west-1.on.aws/?text=How%20is%20the%20weather?) or run locally:

``` bash
cargo lambda watch
```

and then follow the suggested local link.

### Example of running the program
![](images/example.png)

### Logs
![](images/logs.png)

### Some graphs from CloudWatch
![](images/graphs.png)

### P.S.
The project has flaws (you can notice that on the screenshots). Due to the lack of time, I left them as they are since I accomplished the educational purpose of the task. I will certainly check later into the code and find the errors. Another problem I encountered with was extremely slow response from the model if serviced by AWS. Probably the issue is in the arm64 setting.

Also, I excluded the model from the repository not to waste the GitLab resources. If you really want to run the project, download the model from [here](https://huggingface.co/rustformers/pythia-ggml/blob/main/pythia-410m-q4_0-ggjt.bin) and place it in `model/` in the project repository.