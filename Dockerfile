FROM ghcr.io/cargo-lambda/cargo-lambda:latest as builder

WORKDIR /usr/app

COPY . .

RUN cargo clean && cargo lambda build --release --arm64

FROM public.ecr.aws/lambda/provided:al2-arm64

WORKDIR /mp10

COPY --from=builder /usr/app/target/lambda/mp10/bootstrap ./
COPY --from=builder /usr/app/model/pythia-1b-q4_0-ggjt.bin ./model/

ENTRYPOINT ["/mp10/bootstrap"]